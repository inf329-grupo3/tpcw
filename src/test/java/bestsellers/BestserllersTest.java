package bestsellers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Random;

import org.junit.BeforeClass;
import org.junit.Test;

import tpcw.model.Book;
import tpcw.service.Bookstore;

public class BestserllersTest {
	private static final int NUM_EBS = 30;
	private static final int NUM_ITEMS = 10000;

	private static final int NUM_CUSTOMERS = NUM_EBS * 2880;
	private static final int NUM_ADDRESSES = 2 * NUM_CUSTOMERS;
	private static final int NUM_AUTHORS = (int) (.25 * NUM_ITEMS);
	private static final int NUM_ORDERS = (int) (.9 * NUM_CUSTOMERS);

	private static Bookstore bookstore;

	@BeforeClass
	public static void before() {
		bookstore = new Bookstore();
		bookstore.populate((new Random()).nextLong(), System.currentTimeMillis(), NUM_ITEMS, NUM_CUSTOMERS,
				NUM_ADDRESSES, NUM_AUTHORS, NUM_ORDERS);
	}

	@Test
	public void bestsellersInBookListBySubject() {
		List<Book> bestSellers = bookstore.getBestSellers("ARTS"); 
		assertTrue(bestSellers.size() > 0); 
		assertTrue(bookstore.getAllTheBooksBySubject("ARTS").containsAll(bestSellers));
	}

	@Test
	public void bestsellersWithInvalidSubject() {
		assertEquals(0, bookstore.getBestSellers("INVALID").size());
	}

	@Test
	public void bestsellersListSize50() {
		assertEquals(50, bookstore.getBestSellers("ARTS").size());
	}
}
